/*
	Декоратор (англ. Decorator) — структурный шаблон проектирования, предназначенный
	для динамического подключения дополнительного поведения к объекту.
	Шаблон Декоратор предоставляет гибкую альтернативу практике создания подклассов
	с целью расширения функциональности.

	Источник: Wikipedia (Декоратор (шаблон проектирования))
*/

package main

import (
	"errors"
	"fmt"
	"strings"
)

type IngredientAdd interface {
	AddIngredient() (string, error)
}

type PizzaDecorator struct {
	Ingredient IngredientAdd
}

func (p *PizzaDecorator) AddIngredient() (string, error) {
	return "Pizza with the following ingredients: ", nil
}

type Meat struct {
	Ingredient IngredientAdd
}

func (m *Meat) AddIngredient() (string, error) {
	if m.Ingredient == nil {
		return "", errors.New("An IngredientAdd is needed in the Ingredient field of the Meat")
	}
	s, err := m.Ingredient.AddIngredient()
	if err != nil {
		return "", nil
	}
	return fmt.Sprintf("%s %s, ", s, "meat"), nil
}

type Onion struct {
	Ingredient IngredientAdd
}

func (o *Onion) AddIngredient() (string, error) {
	if o.Ingredient == nil {
		return "", errors.New("An IngredientAdd is needed in the Ingredient field of the Meat")
	}
	s, err := o.Ingredient.AddIngredient()
	if err != nil {
		return "", nil
	}
	return fmt.Sprintf("%s %s, ", s, "onion"), nil
}

func main() {
	pizza := &PizzaDecorator{}
	pizzaResult, _ := pizza.AddIngredient()
	expectedText := "Pizza with the following ingredients:"
	if !strings.Contains(pizzaResult, expectedText) {
		fmt.Printf("When calling the add ingredient of the pizza decorator it must return the text %sthe expected text, not '%s'", pizzaResult,
			expectedText)
	}

	meat := &Meat{}
	meatResult, err := meat.AddIngredient()
	if err == nil {
		fmt.Printf("When calling AddIngredient on the meat decorator without"+
			"an IngredientAdd in its Ingredient field must return an error,"+"not a string with '%s'", meatResult)
	}
	meat = &Meat{&PizzaDecorator{}}
	meatResult, err = meat.AddIngredient()
	if err != nil {
		fmt.Println("ERROR: ", err)
	}
	if !strings.Contains(meatResult, "meat") {
		fmt.Printf("When calling the add ingredient of the meat decorator it"+
			"must return a text with the word 'meat', not '%s'", meatResult)
	}
	fmt.Println(meatResult)
}
