package main

import (
	"fmt"
)

type Node struct {
	Val  int
	Next *Node
}

type LinkedList struct {
	Head *Node
	Size int
}

func NewNode(val int) *Node {
	return &Node{
		Val: val,
	}
}

func (list *LinkedList) Insert(value int) {
	n := Node{}
	n.Val = value
	if list.Size == 0 {
		list.Head = &n
		list.Size++
		return
	}
	current := list.Head
	for i := 0; i < list.Size; i++ {
		if current.Next == nil {
			current.Next = &n
			list.Size++
			return
		}
		current = current.Next
	}

}

func (list *LinkedList) Display() {
	if list.Size == 0 {
		fmt.Println("!!! THE LIST IS EMPTY !!!")
		return
	}
	current := list.Head
	for i := 0; i < list.Size; i++ {
		fmt.Println(current.Val)
		current = current.Next
	}
}

func (list *LinkedList) Delete(value int) {
	if list.Size == 0 {
		fmt.Println("!!! THE LIST IS EMPTY !!!")
		return
	}
	current := list.Head
	previous := list.Head
	for i := 0; i < list.Size; i++ {
		if current.Val == value {
			if i == 0 {
				list.Head = current.Next
			} else {
				previous.Next = current.Next
			}
			list.Size--
			fmt.Println("!!! DELETED SUCCESSFULLY !!!")
			return
		}
		previous = current
		current = current.Next
	}
	fmt.Println("!!! THERE IS NO SUCH VALUE !!!")
}

func main() {
	list := LinkedList{}
	for i := 1; i <= 10; i++ {
		list.Insert(i)
	}
	fmt.Println("The list contains of: ")
	list.Display()
	fmt.Println("\n")
	fmt.Println("Let's delete 3: ")
	list.Delete(3)
	fmt.Println("\n")
	fmt.Println("The list after deletion: ")
	list.Display()
	fmt.Println("\n")
	fmt.Println("Now let's add 2021 to the list: ")
	list.Insert(2021)
	list.Display()
	fmt.Println("\n")
	fmt.Println("Let's delete 6 and 2021:")
	list.Delete(6)
	list.Delete(2021)
	fmt.Println("\n")
	fmt.Println("The list after deletion:")
	list.Display()
	fmt.Println("\n")
	fmt.Println("Thank you for your attention!")
}
