/*
POST request in Postman:
{
    "url" : ["http://youtube.com/", "https://facebook.com/", "http://github.com/"]
}
*/

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

type URLs struct {
	Array []string `json:"url"`
}

var clt http.Client = http.Client{}

func sHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		urls := URLs{}
		begin := time.Now()
		decoder := json.NewDecoder(r.Body)
		decoder.Decode(&urls)
		for _, value := range urls.Array {
			makeRequest(value)
		}
		end := time.Since(begin)
		fmt.Println("Synchronous Time (in ms): ", end.Milliseconds())
	}
}

func aHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		urls := URLs{}
		begin := time.Now()
		decoder := json.NewDecoder(r.Body)
		decoder.Decode(&urls)
		for _, value := range urls.Array {
			go makeRequest(value)
			time.Sleep(time.Second)
		}
		end := time.Since(begin)
		fmt.Println("Asynchronous Time (in ms): ", end.Milliseconds()-1000)
	}
}

func makeRequest(url string) {
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println("REQUEST ERROR: ", err)
		return
	}
	_, doErr := clt.Do(request)
	if doErr != nil {
		fmt.Println("GET REQUEST ERROR: ", err)
		return
	}
}

func main() {
	Rt := mux.NewRouter()
	Rt.HandleFunc("/sync", sHandler)
	Rt.HandleFunc("/async", aHandler)
	http.ListenAndServe(":8080", Rt)
}
