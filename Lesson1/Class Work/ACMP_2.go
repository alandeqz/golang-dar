package main

import "fmt"

func main() {
	var n = 0
	fmt.Scanln (&n)
	sum := 0
	for i := 1; i <= n; i++ {
		sum = sum + i;
	}
	fmt.Println (sum)
}