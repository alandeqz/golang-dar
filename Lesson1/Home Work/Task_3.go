package main

import "fmt"

func main() {

	fmt.Println (fibonacci (10))
}

func fibonacci (n int) int {
	if n <= 1 {
		return n
	}
	var result = 1
	for i := 1; i <= n; i++ {
		result = result * i
	}
	return result
}