package main

import "fmt"

func main() {
	var m = min (0, 1, 2, 3)
	fmt.Println (m)
}

func min (a int, b int, c int, d int) int {
	if a <= b && a <= c && a <= d {
		return a
	} else if b <= a && b <= c && b <= d {
		return b
	} else if c <= a && c <= b && c <= d {
		return c
	} else {
		return d
	}
}