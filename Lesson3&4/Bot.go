package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type From struct {
	ID           int64   `json:"id"`
	IsBot        bool    `json:"is_bot"`
	FirstName    string  `json:"first_name"`
	Username     string  `json:"username"`
	LanguageCode *string `json:"language_code,omitempty"`
}

type Chat struct {
	ID        int64  `json:"id"`
	FirstName string `json:"first_name"`
	Username  string `json:"username"`
	Type      string `json:"type"`
}

type Message struct {
	MessageID int64  `json:"message_id"`
	From      From   `json:"from"`
	Chat      Chat   `json:"chat"`
	Date      int64  `json:"date"`
	Text      string `json:"text"`
}

type Result struct {
	UpdateID int64   `json:"update_id"`
	Message  Message `json:"message"`
}

type Update struct {
	Ok     bool     `json:"ok"`
	Result []Result `json:"result"`
}

type Client struct {
	ConnectionUrl ConnectionString
	clt           http.Client
}

type ConnectionString string

type GetMeResult struct {
	ID        int    `json:"id"`
	IsBot     bool   `json:"is_bot"`
	FirstName string `json:"first_name,omitempty"`
	Username  string `json:"username"`
}

type GetMeResponse struct {
	OK     bool        `json:"ok"`
	Result GetMeResult `json:"result"`
}

func main() {
	clt := http.Client{}
	request, err := http.NewRequest("GET", "https://api.telegram.org/bot1511874232:AAHQHW0OfNrT8-7L5gUnMQZeeh2zteoQ9RA/getUpdates?offset=-1", nil)
	if err != nil {
		fmt.Println("ERROR ON GET REQUEST: ", err)
		return
	}
	resp, err := clt.Do(request)
	if err != nil {
		fmt.Println("ERROR ON GETTING REQUEST: ", err)
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("ERROR IN READING THE BODY: ", err)
		return
	}

	// fmt.Println(string(body))

	var upd Update

	er := json.Unmarshal(body, &upd)
	if er != nil {
		fmt.Println("UNMARSHAL ERROR: ", er)
		return
	}

	if len(upd.Result) == 0 {
		return
	}

	msg := upd.Result[0].Message.Text
	id := upd.Result[0].Message.Chat.ID

	if strings.ToLower(msg) == "hello" {
		msg = "Hello! :)"
	} else if strings.ToLower(msg) == "how are you?" {
		msg = "Nice, how are you? :)"
	} else if strings.ToLower(msg) == "what's your name?" {
		msg = "I don't have a name still :("
	} else {
		msg = "Sorry, I don't know the answer :("
	}

	url := fmt.Sprintf("https://api.telegram.org/bot1511874232:AAHQHW0OfNrT8-7L5gUnMQZeeh2zteoQ9RA/sendMessage?chat_id=%d&text=%s", id, msg)

	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		fmt.Println("ERROR ON POST REQUEST: ", err)
		return
	}
	_, e := clt.Do(req)
	if e != nil {
		fmt.Println("ERROR ON GETTING REQUEST: ", e)
		return
	}
}
